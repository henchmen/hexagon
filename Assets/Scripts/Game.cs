using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour {
	public Transform Ship;
	public Transform Block;
	public Transform BlockBreakable;
	public Material Block_1_single;
	public Material Block_1_left;
	public Material Block_1_right;
	public Material Block_1_middle_2;
	public Material Temp;

	public AudioClip Explosion;
	public AudioClip Lost;
	public AudioClip Restart;

	private int _velocity;
	private bool _dead;
	private bool _guiOn;
	private int _leftBorder;
	private int _rightBorder;
	private int _topBorder;
	private int _bottomBorder;
	private int _frameCounter;

	private List<GameObject> _platforms;
	private Transform platform;
	private Transform ship;

	public int velocity 
	{
		get { return _velocity; }
		set { _velocity = value; }
	}

	public int leftBorder 
	{
		get { return _leftBorder; }
		set { _leftBorder = value; }
	}

	public int rightBorder 
	{
		get { return _rightBorder; }
		set { _rightBorder = value; }
	}

	public int topBorder 
	{
		get { return _topBorder; }
		set { _topBorder = value; }
	}

	public int bottomBorder 
	{
		get { return _bottomBorder; }
		set { _bottomBorder = value; }
	}

	public bool dead 
	{
		get { return _dead; }
	}

	void Start () 
	{
		_velocity = 5;
		_dead = false;
		_leftBorder = 0;
		_rightBorder = 6;
		_topBorder = 12;
		_bottomBorder = -12;
		_guiOn = false;
		_platforms = new List<GameObject>();

		StartGame();
	}

	void Update () 
	{
		if (Input.GetAxis("Fire1") != 0 && _dead)
		{
			cleanUpAndStartNewGame();
		}

	}
/*
	void FixedUpdate()
	{
		_frameCounter++;

		if (_frameCounter == 10 && !_dead)
		{
			createPlatform();
			_frameCounter = 0;
		}
		*/

			/*
		if (_platforms.Count == 0 || _platforms[_platforms.Count - 1].transform.position.y >= _bottomBorder + 1)
		{
			createPlatform();
		}
	}
	*/

	IEnumerator SpawnPlatforms() 
	{
		while(!_dead) 
		{
			createPlatform();
			yield return new WaitForSeconds(0.19f);
		}
	}

	public void createPlatform()
	{
		GameObject platform = new GameObject("Platform");
		platform.transform.position = new Vector3(0, _bottomBorder, 0);
		platform.transform.localScale = new Vector3(1, 1, 0);
		platform.transform.parent = transform;
		platform.AddComponent<Platform>();
		platform.tag = "Movable";
		_platforms.Add(platform);
	}

	public void StartGame() 
	{
		ship = Instantiate(Ship, new Vector3(3, 4, 0), Quaternion.identity) as Transform;	
		ship.rotation = Quaternion.AngleAxis(180, Vector3.up);
		ship.parent = transform;
		StartCoroutine("SpawnPlatforms");
		//InvokeRepeating("createPlatform", 0.19f, 0.19f);
	}

	public void GameOver() 
	{
		audio.PlayOneShot(Explosion);
		audio.PlayOneShot(Lost);
		_dead = true;
		_velocity = 0;
		_guiOn = true;
	}

	public void cleanUpAndStartNewGame() 
	{
		Destroy(ship.gameObject);
		GameObject[] movables;
		movables = GameObject.FindGameObjectsWithTag("Movable");
		foreach (GameObject movable in movables) 
		{
			Destroy(movable);
		}
		_dead = false;
		_guiOn = false;
		_velocity = 5;
		audio.PlayOneShot(Restart);
		StartGame();
	}

	public void OnGUI() 
	{
		if (_guiOn) 
		{
			if (GUI.Button (new Rect (10, 10, 150, 100), "Continue?")) 
			{
				cleanUpAndStartNewGame();
			}
		}
	}
}
