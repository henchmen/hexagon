using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {
	private Game _game;
	private int _x;
	private bool[] _blocks; 
	private Transform block;

	void Start () {
		_game = transform.parent.GetComponent<Game>();

		_blocks = GetBlockLayout(_game.rightBorder);

		int amountOf_blocks = _blocks.Length;
		for (int i = 0; i < amountOf_blocks; i++) 
		{
			if (_blocks[i])
			{
				block = Instantiate(_game.Block) as Transform;
				if (GetRandomBoolean()) 
				{
					block.gameObject.renderer.material = _game.Block_1_middle_2;
				}

				if (i > 0 && i < amountOf_blocks - 1 && !_blocks[i - 1] && !_blocks[i + 1])
				{
					block.gameObject.renderer.material = _game.Block_1_single;
				}
				else if (i > 0 && !_blocks[i - 1]) 
				{
					block.gameObject.renderer.material = _game.Block_1_left;
				}
				else if (i < amountOf_blocks - 1 && !_blocks[i + 1])
				{
					block.gameObject.renderer.material = _game.Block_1_right;
				}

				block.gameObject.renderer.material = _game.Temp;
				block.parent = transform;
				block.localScale = new Vector3(1, 1, 1);
				block.localPosition = new Vector3(i, 0, 0);
			}
		}
	}
	
	void Update () 
	{
		transform.Translate(0, Time.deltaTime * _game.velocity, 0);
		if (transform.position.y > _game.topBorder) 
		{
			Destroy(gameObject);
		}
	}

	public bool[] GetBlockLayout(int size) 
	{
		bool[] blockLayout = new bool[size];
		bool allTrue = true;
		for (int i = 0; i < size; i++) 
		{
			blockLayout[i] = GetRandomBoolean();
			if (blockLayout[i] == false) {
				allTrue = false;
			}
		}
		if (allTrue) {
			int i = Random.Range(0, size);
			blockLayout[i] = false;
		}

		return blockLayout;
	}

	// TODO: In een helper class zetten
	public static bool GetRandomBoolean() 
	{
		return (Random.value > 0.5f);
	}
}
