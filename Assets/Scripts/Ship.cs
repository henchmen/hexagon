using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	public AudioClip MoveLeft;
	public AudioClip MoveRight;

	private Game _game;
	private bool _isAxisInUse = false;

	void Start () {
		_game = transform.parent.GetComponent<Game>();
		//transform.gameObject.Rigidbody.isKinematic = true;
	}
	
	void Update () {
		Vector3 newPosition = transform.position;

		if (!_game.dead) {
			if (Input.GetAxisRaw("Horizontal") < 0 && transform.position.x > _game.leftBorder)
			{
				if (_isAxisInUse == false)
				{
					audio.PlayOneShot(MoveLeft);
					newPosition.x--;
					_isAxisInUse = true;
				}
			}
			if (Input.GetAxisRaw("Horizontal") > 0 && transform.position.x < _game.rightBorder - 1)
			{
				if (_isAxisInUse == false) 
				{
					audio.PlayOneShot(MoveRight);
					newPosition.x++;
					_isAxisInUse = true;
				}
			}

			if (Input.GetAxisRaw("Horizontal") == 0)
			{
				_isAxisInUse = false;
			}

			transform.position = newPosition;
		}
	}

	void OnCollisionEnter(Collision collision) 
	{
		_game.GameOver();
	}
}
